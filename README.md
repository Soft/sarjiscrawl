# Web comic archiver for sarjis.info

Web comic archiver for sarjis.info, a Finnish web comic aggregator service.

```
sarjiscrawl

Usage:
  sarjiscrawl [--max-entries=<num>] [--direction=forward] <begin> <output>
  sarjiscrawl -h | --help
```

