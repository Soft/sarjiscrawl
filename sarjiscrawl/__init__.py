#!/usr/bin/env python3

"""sarjiscrawl

Usage:
  sarjiscrawl [--max-entries=<num>] [--direction=forward] <begin> <output>
  sarjiscrawl -h | --help
"""

import scrapy
import logging
import os.path
from datetime import datetime
from urllib.parse import urlparse
from scrapy.crawler import CrawlerProcess
from scrapy.pipelines.files import FilesPipeline
from docopt import docopt
from enum import Enum

__version__ = "0.1"

SETTINGS = {
    "LOG_LEVEL": logging.INFO,
    "ITEM_PIPELINES": {
        "sarjiscrawl.ComicPipeline": 1
    }
}

MONTHS = dict((v, k) for k, v in enumerate(
    ["tammikuu", "helmikuu", "maaliskuu", "huhtikuu",
     "toukokuu", "kesäkuu", "heinäkuu", "elokuu",
     "syyskuu", "lokakuu", "marraskuu", "joulukuu"], 1))

class Direction(Enum):
    FORWARD = 0
    BACKWARD = 1

class Comic(scrapy.Item):
    id = scrapy.Field()
    title = scrapy.Field()
    date = scrapy.Field()
    url = scrapy.Field()

class SarjisSpider(scrapy.Spider):
    """Spider for crawling comic strips from sarjis.info"""

    name = "sarjis"
    allowed_domains = ["sarjis.info"]

    def __init__(self, url, max_entries=None, direction=Direction.FORWARD):
        super().__init__()
        self.start_urls = [url]
        self.max_entries = max_entries
        self.direction = direction
        self.processed = 0

    def parse(self, response):
        id = next(int(p) for p in reversed(urlparse(response.url).path.split("/")) if p)
        comic = response.css(".sarjakuva_vasen > a > img::attr(src)").extract_first()
        title = response.css(".sarjakuva_vasen > h1::text").extract_first()

        month, year = response.css(".kalenteri .kk > td::text").extract_first().strip().split(" ")
        day = response.css(".kalenteri .pvalittu > a::text").extract_first()

        if self.direction is Direction.FORWARD:
            next_ = response.css("#seuraava::attr(href)").extract_first()
        else:
            next_ = response.css("#edellinen::attr(href)").extract_first()

        if id and comic and title:
            logging.info("Comic %s", id)
            parts = title.split("-")
            title = parts[-1].strip() if len(parts) > 1 else None
            if year and month and day:
                date = datetime(int(year), MONTHS[month], int(day))
            else:
                date = None
            yield Comic(id=id,
                        date=date,
                        title=title,
                        url=comic)

        self.processed += 1
        if next_ and self.should_continue:
            yield scrapy.Request(next_)

    @property
    def should_continue(self):
        if self.max_entries is None:
            return True
        else:
            return self.processed < self.max_entries

class ComicPipeline(FilesPipeline):
    """Pipeline for saving comics"""

    def file_path(self, request, response=None, info=None):
        return request.meta["filename"]

    def get_media_requests(self, item, info):
        yield scrapy.Request(url=item["url"], meta={
            "filename": self.__name(item)
        })

    def __name(self, item):
        date = item["date"].strftime("%Y-%m-%d") if item["date"] is not None else "?"
        title = "-" + item["title"].replace("/", "⁄") if item["title"] is not None else ""
        ext = os.path.splitext(item["url"])[1]
        return "{:06d}-{}{}{}".format(item["id"], date, title, ext)


def main():
    arguments = docopt(__doc__)

    if arguments["--max-entries"] is not None:
        max_entries = int(arguments["--max-entries"])
    else:
        max_entries = None

    direction = {
        "forward": Direction.FORWARD,
        "backward": Direction.BACKWARD
    }.get(arguments["--direction"], Direction.FORWARD)

    SETTINGS["FILES_STORE"] = arguments["<output>"]

    runner = CrawlerProcess(SETTINGS)
    runner.crawl(SarjisSpider,
                 arguments["<begin>"],
                 max_entries=max_entries,
                 direction=direction)
    runner.start()

if __name__ == "__main__":
    main()
