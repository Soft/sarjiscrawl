from setuptools import setup

import sarjiscrawl

with open("README.md", "r") as handle:
    long_description = handle.read()

setup(name="sarjiscrawl",
      version=sarjiscrawl.__version__,
      description="Crawler for comics from sarjis.info",
      long_description=long_description,
      packages = ["sarjiscrawl"],
      entry_points={
          "console_scripts": [
              "sarjiscrawl = sarjiscrawl:main"
          ]
      },
      install_requires=[
          "scrapy==1.4.0",
          "docopt==0.6.2"
      ],
      keywords=["crawler", "comics"],
      license="GPL-3.0",
      zip_safe=True)
